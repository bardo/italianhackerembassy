import json
import os.path

import leds
import st3m.run
from ctx import Context
from st3m.application import Application, ApplicationContext
from st3m.input import InputState


class RGBColor:
    def __init__(self, r, g, b):
        self.r: float = r
        self.g: float = g
        self.b: float = b


flag_green = RGBColor(0, 0.546875, 0.26953125)
flag_white = RGBColor(1, 1, 1)
flag_red = RGBColor(0.80078125, 0.12890625, 0.1640625)

nick_config = {
    "color": RGBColor(0, 0, 0),
    "font": "Camp Font 1",
    "size": 32,
}

config_file = "/flash/nick.json"


class ItalianHackerEmbassy(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

        if os.path.exists(config_file):
            with open(config_file) as fp:
                config = json.load(fp)

        try:
            self.nick = str(config["name"])
        except Exception:
            self.nick = ""

        self.show_nick = False

    def draw(self, ctx: Context) -> None:
        # Display
        ctx.rgb(flag_green.r, flag_green.g, flag_green.b).rectangle(-120, -120, 60, 240).fill()
        ctx.rgb(flag_white.r, flag_white.g, flag_white.b).rectangle(-60, -120, 120, 240).fill()
        ctx.rgb(flag_red.r, flag_red.g, flag_red.b).rectangle(60, -120, 60, 240).fill()

        # Green LEDs
        for led in range(22, 37):
            leds.set_rgb(led, 0, 255, 0)

        # White LEDs
        for led in list(range(0, 4)) + list(range(19, 22)) + list(range(37, 40)):
            leds.set_rgb(led, 255, 255, 255)

        # Red LEDs
        for led in range(4, 19):
            leds.set_rgb(led, 255, 0, 0)

        leds.update()

        if self.show_nick and self.nick:
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.font = nick_config["font"]
            ctx.font_size = nick_config["size"]
            ctx.rgb(nick_config["color"].r, nick_config["color"].g, nick_config["color"].b).move_to(0, 0).text(self.nick)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        if self.nick and self.input.buttons.app.middle.pressed:
            self.show_nick = not self.show_nick


if __name__ == "__main__":
    st3m.run.run_responder(ItalianHackerEmbassy())
